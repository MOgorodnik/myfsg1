/* Setup blank page controller */
var FSG = angular.module('MetronicApp', []);
FSG.controller('LogsController', ['$rootScope', '$scope', 'settings', function($rootScope, $scope, settings) {
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
}]);

FSG.controller('logsTableController', function ($scope, $http) {

/*  $http.get('http://mysafeinfo.com/api/data?list=englishmonarchs&format=json').then(successCallback, errorCallback);
    $http.get('./js/json/logs.json').then(successCallback, errorCallback);
    function successCallback(data){
        //success code
        $scope.logs = data;
     }
    function errorCallback(error){
        //error code
        console.log("error");
    }*/
        $scope.logs = [];
        $http.get('./js/json/logs.json').success(function(data) {
        $scope.logs = data;
        //$scope.myLogs = data;
        //$scope.logs = data.logs;
    });
});

