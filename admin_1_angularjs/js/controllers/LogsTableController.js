/* Setup blank page controller */

var FSG = angular.module('MetronicApp.angularWay.withOptions', ['datatables', 'ngResource']);
FSG.controller('logsTableController', logsTableController, ['$rootScope', '$scope', 'settings', function($rootScope, $scope, settings) {
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
    $scope.user = "ready!!!!!!!!!!!!!!!";

}]);

function logsTableController($resource, DTOptionsBuilder) {
    var vm = this;
    vm.logs = [];
    vm.dtOptions = {
        // paginationType: 'full_numbers',
        displayLength: 5,
        language: {
            "info": "_TOTAL_ Logs", // info
            "lengthMenu": "Records Per Page _MENU_", // select
            "search": "Search:" // filter
        },
        dom: "<'dataTable_wrapper-head'<'col-xs-3'i><'col-xs-9'<'pull-right'l>>><'table-responsive'rt><'dataTable_wrapper-foot'<'col-md-12'p>>",

        bStateSave: true, // save datatable state(pagination, sort, etc) in cookie. false

        lengthMenu: [
            [5, 10, 20, -1],
            [5, 10, 20, "All"] // change per page values here
        ],
        pagingType: "bootstrap_full_number",
        autoWidth: false,
        columnDefs: [
            {  // set default column settings
                "width": 40,
                "targets": 0,
                "orderable": false
            }
            ,{
                "width": 60,
                "targets": 1
            }
            ,{
                "width": 215,
                "targets": 2
            }
            ,{
                "width": 320,
                "targets": 8
            }
            ,{
                "width": 175,
                "targets": 9
            }
        ]
    };

    // working variant using .with***
    //////////////////////////////////////////////////////
    // vm.dtOptions = DTOptionsBuilder.newOptions()
    //     .withLanguage({
    //         "sEmptyTable":     "No data available in table",
    //         "sInfo":           "_TOTAL_ Logs",
    //         "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
    //         "sInfoFiltered":   "(filtered from _MAX_ total entries)",
    //         "sInfoPostFix":    "",
    //         "sInfoThousands":  ",",
    //         "sLengthMenu":     "Records Per Page _MENU_",
    //         "sLoadingRecords": "Loading...",
    //         "sProcessing":     "Processing...",
    //         "sSearch":         "Search:",
    //         "sZeroRecords":    "No matching records found",
    //         "oPaginate": {
    //             "sFirst":    "<<",
    //             "sLast":     ">>",
    //             "sNext":     ">",
    //             "sPrevious": "<"
    //         },
    //         "oAria": {
    //             "sSortAscending":  ": activate to sort column ascending",
    //             "sSortDescending": ": activate to sort column descending"
    //         }
    //     })
    //     .withPaginationType('full_numbers')
    //     .withDOM("<'dataTable_wrapper-head'<'col-xs-3'i><'col-xs-9'<'pull-right'l>>><'table-responsive'rt><'dataTable_wrapper-foot'<'col-md-12'p>>");
        // .withFixedColumns({
        //     // leftColumns: 1,
        //     // rightColumns: 1,
        //     "columnDefs": [
        //         { width: 200, targets: 0 }
        //     ]
        // });
    $resource('./js/json/logs.json').query().$promise.then(function(logs) {
        vm.logs = logs;
    });
}
